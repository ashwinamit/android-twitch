package zebra.com.example.android.twitchandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import zebra.com.example.android.twitchandroid.R;
import zebra.com.example.android.twitchandroid.fragments.GameDetailFragment;
import zebra.com.example.android.twitchandroid.fragments.GameListFragment;
import zebra.com.example.android.twitchandroid.fragments.StreamDetailFragment;


public class MainActivity extends ActionBarActivity implements
        GameListFragment.GameListFragmentContainer,
        GameDetailFragment.GameDetailFragmentContainer,
        StreamDetailFragment.StreamDetailFragmentContainer {

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.container);
        if (fragment == null) {
            fragment = GameListFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment)
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void gameSelected(String gameName) {
        Log.v(LOG_TAG, gameName);
        GameDetailFragment fragment = GameDetailFragment.newInstance(gameName);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void streamSelected(String streamName) {
        Log.v(LOG_TAG, streamName);
        StreamDetailFragment fragment = StreamDetailFragment.newInstance(streamName);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void linkSelected(String link) {
        Log.v(LOG_TAG, link);
        Intent i = new Intent(this, StreamActivity.class);
        i.putExtra(StreamActivity.STREAM_URL_KEY, link);
        startActivity(i);
    }
}
