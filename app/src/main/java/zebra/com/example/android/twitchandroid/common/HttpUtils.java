package zebra.com.example.android.twitchandroid.common;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by zebra on 3/13/2015.
 */
public class HttpUtils {

    private static final String LOG_TAG = HttpUtils.class.getSimpleName();

    /**
     * Consumes connection and disconnects it
     *
     * @param unconnectedUrlConnection HttpURLConnection that has not be "connect" called yet.
     * @return @Nullable String
     * @throws IOException
     */
    @Nullable
    private static String readUrl(@NonNull HttpURLConnection unconnectedUrlConnection) throws IOException {
        BufferedReader reader = null;
        String result = null;

        try {
            unconnectedUrlConnection.connect();

            InputStream inputStream = unconnectedUrlConnection.getInputStream();
            StringBuilder buffer = new StringBuilder();
            if (inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
            }
            if (buffer.length() == 0) {
                return null;
            }
            result = buffer.toString();
        } finally {
            unconnectedUrlConnection.disconnect();
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error closing stream", e);
                }
            }
        }
        return result;
    }

    public static String httpGet(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        return readUrl(urlConnection);

    }

    public static String twitchGet(URL url, String twitchApiLevel) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.addRequestProperty("Accept", twitchApiLevel);
        return readUrl(urlConnection);
    }
}
