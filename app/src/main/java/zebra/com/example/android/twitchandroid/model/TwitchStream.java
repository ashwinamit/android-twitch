package zebra.com.example.android.twitchandroid.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zebra on 3/13/2015.
 */
public class TwitchStream extends TwitchChannel {

    private final int viewers;

    public TwitchStream(String name, String displayName, int viewers) {
        super(name, displayName);
        this.viewers = viewers;
    }

    public static TwitchStream[] parseFromJson(String json) throws JSONException {
        JSONObject response = new JSONObject(json);
        JSONArray streamsJson = response.getJSONArray("streams");
        TwitchStream[] streams = new TwitchStream[streamsJson.length()];
        for (int i = 0; i < streamsJson.length(); i++) {
            JSONObject stream = streamsJson.getJSONObject(i);
            int viewers = stream.getInt("viewers");
            JSONObject channel = stream.getJSONObject("channel");
            String name = channel.getString("name");
            String displayName = channel.getString("display_name");
            streams[i] = new TwitchStream(name, displayName, viewers);
        }
        return streams;
    }

    public int getViewers() {
        return viewers;
    }

}
