package zebra.com.example.android.twitchandroid.tasks;

import android.net.Uri;

import org.json.JSONException;

import zebra.com.example.android.twitchandroid.common.TwitchConstants;
import zebra.com.example.android.twitchandroid.model.TwitchStream;

/**
 * Created by zebra on 3/13/2015.
 */
public class DownloadStreamList extends TwitchDownloader<TwitchStream> {

    private static final String LOG_TAG = DownloadStreamList.class.getSimpleName();

    public DownloadStreamList(DownloadListener<TwitchStream> listener) {
        super(listener);
    }


    @Override
    protected String getBaseUrl() {
        return TwitchConstants.STREAMS_BASE;
    }

    @Override
    protected String getTwitchApiLevel() {
        return TwitchConstants.API_VERSION_3;
    }

    @Override
    protected TwitchStream[] parseFromJson(String json) throws JSONException {
        return TwitchStream.parseFromJson(json);
    }

    public static class StreamQuery implements TwitchDownloader.AsyncParameters {
        public final String game;
        public final int offset;
        public final int limit;

        public StreamQuery(String query, int offset, int limit) {
            this.game = query;
            this.offset = offset;
            this.limit = limit;
        }

        @Override
        public void appendAsQuery(Uri.Builder base) {
            base.appendQueryParameter("game", game);
            base.appendQueryParameter("offset", "" + offset);
            base.appendQueryParameter("limit", "" + limit);
        }
    }
}
