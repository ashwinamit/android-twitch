package zebra.com.example.android.twitchandroid.common;

/**
 * Created by zebra on 3/13/2015.
 * TODO: Consider refactoring this to pull data on app startup from "_links" attribute?
 */
public class TwitchConstants {

    public static final String API_VERSION_3 = "application/vnd.twitchtv.v3+json";
    public static final String API_VERSION_2 = "application/vnd.twitchtv.v2+json";

    public static final String GAMES_API_BASE = "https://api.twitch.tv/kraken/games/top";
    public static final String STREAMS_BASE = "https://api.twitch.tv/kraken/streams";

}
