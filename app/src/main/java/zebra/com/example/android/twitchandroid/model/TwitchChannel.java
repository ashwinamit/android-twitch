package zebra.com.example.android.twitchandroid.model;

/**
 * Created by zebra on 3/13/2015.
 */
public class TwitchChannel implements TwitchEntity {

    private final String name;
    private final String displayName;

    public TwitchChannel(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

}
