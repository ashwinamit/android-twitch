package zebra.com.example.android.twitchandroid.tasks;

import android.net.Uri;

import org.json.JSONException;

import zebra.com.example.android.twitchandroid.common.TwitchConstants;
import zebra.com.example.android.twitchandroid.model.TwitchGame;

/**
 * Created by zebra on 3/13/2015.
 */
public class DownloadGameList extends TwitchDownloader<TwitchGame> {

    public DownloadGameList(DownloadListener<TwitchGame> listener) {
        super(listener);
    }

    @Override
    protected String getBaseUrl() {
        return TwitchConstants.GAMES_API_BASE;
    }

    @Override
    protected String getTwitchApiLevel() {
        return TwitchConstants.API_VERSION_3;
    }

    @Override
    protected TwitchGame[] parseFromJson(String json) throws JSONException {
        return TwitchGame.parseFromJson(json);
    }

    public static class GameQuery implements AsyncParameters {
        final int offset;
        final int limit;

        public GameQuery(int offset, int limit) {
            this.offset = offset;
            this.limit = limit;
        }

        @Override
        public void appendAsQuery(Uri.Builder base) {
            base.appendQueryParameter("offset", "" + offset);
            base.appendQueryParameter("limit", "" + limit);
        }
    }
}
