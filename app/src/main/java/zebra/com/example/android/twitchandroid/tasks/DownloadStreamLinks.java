package zebra.com.example.android.twitchandroid.tasks;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import zebra.com.example.android.twitchandroid.common.HttpUtils;

/**
 * Created by zebra on 3/14/2015.
 * Don't override TwitchDownloader since the AP for getting stream links is different
 * from normal api.twitch
 * <p/>
 * Taken from https://gist.github.com/baderj/8593472
 */
public class DownloadStreamLinks extends AsyncTask<String, Void, Map<String, String>> {


    public interface LinksDownloadedListener {
        public void streamLinksDownloaded(Map<String, String> streamInfo);
    }

    private static final String LOG_TAG = DownloadStreamList.class.getSimpleName();
    /**
     * Expects 4 arguments (channel_display_name, token, sig, (int) random)
     */
    private static final String USHER_API = "http://usher.twitch.tv/api/channel/hls/%s.m3u8"
            + "?player=twitchweb"
            + "&token=%s&sig=%s&allow_audio_only=true&allow_source=true"
            + "&type=any&p=%d";
    /**
     * Expects 1 string argument, channel_display_name
     */
    private static final String TOKEN_API = "http://api.twitch.tv/api/channels/%s/access_token";
    private final LinksDownloadedListener listener;

    public DownloadStreamLinks(LinksDownloadedListener listener) {
        this.listener = listener;
    }

    @NonNull
    private Map<String, String> parseUsherResponse(@NonNull String usherResponse) {
        String[] lines = usherResponse.split("\n");
        Map<String, String> urls = new HashMap<>();
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].indexOf("http") == 0) {
                urls.put(lines[i - 1], lines[i]);
            }
        }
        return urls;
    }

    @Override
    @Nullable
    protected Map<String, String> doInBackground(String... params) {
        if (params.length == 0) {
            return null;
        }
        String channelName = params[0];
        Map<String, String> response = null;
        try {
            URL tokenUrl = new URL(String.format(TOKEN_API, channelName));
            JSONObject tokenResponse = new JSONObject(HttpUtils.httpGet(tokenUrl));
            Log.v(LOG_TAG, tokenResponse.toString());
            String sig = tokenResponse.getString("sig");
            String token = tokenResponse.getString("token");
            int random = (int) (Math.random() * 100000);

            URL usherUrl = new URL(String.format(USHER_API, channelName, token, sig, random));
            String usherResponse = HttpUtils.httpGet(usherUrl);
            Log.v(LOG_TAG, usherResponse);
            response = parseUsherResponse(usherResponse);
        } catch (IOException | JSONException e) {
            Log.e(LOG_TAG, "Error download streams", e);
        }
        return response;
    }

    @Override
    protected void onPostExecute(Map<String, String> links) {
        listener.streamLinksDownloaded(links);
    }
}
