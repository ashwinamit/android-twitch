package zebra.com.example.android.twitchandroid.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zebra on 3/13/2015.
 */
public class TwitchGame implements TwitchEntity, Parcelable {

    public static final Parcelable.Creator<TwitchGame> CREATOR
            = new Parcelable.Creator<TwitchGame>() {
        public TwitchGame createFromParcel(Parcel in) {
            return new TwitchGame(in);
        }

        public TwitchGame[] newArray(int size) {
            return new TwitchGame[size];
        }
    };
    private final int id;
    private final String name;
    private final int channels;
    private final int viewers;

    public TwitchGame(int id, String name, int channels, int viewers) {
        this.id = id;
        this.name = name;
        this.channels = channels;
        this.viewers = viewers;
    }

    private TwitchGame(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.channels = in.readInt();
        this.viewers = in.readInt();
    }

    /**
     * https://github.com/justintv/Twitch-API/blob/master/v3_resources/games.md
     *
     * @param jsonResult json
     * @return TwitchGame[]
     * @throws org.json.JSONException
     */
    public static TwitchGame[] parseFromJson(String jsonResult) throws JSONException {
        JSONObject response = new JSONObject(jsonResult);
        JSONArray unparsedGameArray = response.getJSONArray("top");
        TwitchGame[] games = new TwitchGame[unparsedGameArray.length()];
        for (int i = 0; i < unparsedGameArray.length(); i++) {
            JSONObject obj = unparsedGameArray.getJSONObject(i);
            int viewers = obj.getInt("viewers");
            int channels = obj.getInt("channels");
            JSONObject gameJson = obj.getJSONObject("game");
            int id = gameJson.getInt("_id");
            String name = gameJson.getString("name");
            games[i] = new TwitchGame(id, name, channels, viewers);
        }
        return games;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getChannels() {
        return channels;
    }

    public int getViewers() {
        return viewers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(channels);
        dest.writeInt(viewers);
    }

}
