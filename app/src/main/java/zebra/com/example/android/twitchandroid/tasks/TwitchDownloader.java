package zebra.com.example.android.twitchandroid.tasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import zebra.com.example.android.twitchandroid.common.HttpUtils;
import zebra.com.example.android.twitchandroid.model.TwitchEntity;

/**
 * Created by zebra on 3/14/2015.
 * TODO: Builder it? Just need a way to express the parseAsJson function choice in the builder
 */
public abstract class TwitchDownloader<E extends TwitchEntity> extends AsyncTask<TwitchDownloader.AsyncParameters, Void, E[]> {

    //TODO: Figure out how to make it that I don't need to declare entity in this
    public static interface DownloadListener<E extends TwitchEntity> {
        public void downloadComplete(E[] result);
    }

    public static interface AsyncParameters {
        void appendAsQuery(Uri.Builder base);
    }

    private static final String LOG_TAG = TwitchDownloader.class.getSimpleName();
    private DownloadListener<E> listener;

    public TwitchDownloader(DownloadListener<E> listener) {
        this.listener = listener;
    }

    @Override
    @Nullable
    public E[] doInBackground(AsyncParameters... params) {
        if (params.length == 0) {
            return null;
        }
        E[] entities = null;
        try {

            URL url = getURL(params[0]);
            String response = HttpUtils.twitchGet(url, getTwitchApiLevel());
            if (response != null) {
                entities = parseFromJson(response);
            }
        } catch (IOException | JSONException e) {
            Log.e(LOG_TAG, "Error downloading", e);
        }
        return entities;
    }

    @Override
    protected void onPostExecute(E[] twitchEntities) {
        listener.downloadComplete(twitchEntities);
    }

    private URL getURL(AsyncParameters parameter) throws MalformedURLException {
        Uri.Builder builder = Uri.parse(getBaseUrl()).buildUpon();
        parameter.appendAsQuery(builder); //will mutate this copy
        return new URL(builder.toString());
    }

    protected abstract String getBaseUrl();

    protected abstract String getTwitchApiLevel();

    protected abstract E[] parseFromJson(String json) throws JSONException;
}
