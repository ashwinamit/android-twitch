package zebra.com.example.android.twitchandroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Map;

import zebra.com.example.android.twitchandroid.tasks.DownloadStreamLinks;

/**
 * Created by zebra on 3/14/2015.
 */
public class StreamDetailFragment extends ListFragment implements DownloadStreamLinks.LinksDownloadedListener {

    public interface StreamDetailFragmentContainer {
        public void linkSelected(String link);
    }

    private static final String LOG_TAG = StreamDetailFragment.class.getSimpleName();
    private static final String STREAM_NAME_KEY = "stream_name_key";

    private StreamDetailFragmentContainer listener;
    private String streamName;
    private ArrayAdapter<String> mStreamLinks;

    public StreamDetailFragment() {
    }

    public static StreamDetailFragment newInstance(String streamName) {
        Bundle args = new Bundle();
        args.putString(STREAM_NAME_KEY, streamName);

        StreamDetailFragment fragment = new StreamDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (StreamDetailFragmentContainer) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement StreamDetailFragmentContainer");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            streamName = getArguments().getString(STREAM_NAME_KEY);
        } else {
            streamName = savedInstanceState.getString(STREAM_NAME_KEY);
        }
        mStreamLinks = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        setListAdapter(mStreamLinks);
        downloadLinks();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STREAM_NAME_KEY, streamName);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String streamLink = mStreamLinks.getItem(position);
        Log.v(LOG_TAG, "Selected stream " + streamLink);
        listener.linkSelected(streamLink);
    }

    private void downloadLinks() {
        new DownloadStreamLinks(this).execute(streamName);
    }

    @Override
    public void streamLinksDownloaded(Map<String, String> streamInfo) {
        mStreamLinks.clear();
        if (streamInfo != null) {
            for (String s : streamInfo.keySet()) {
                mStreamLinks.add(streamInfo.get(s));
            }
        }
    }
}
