package zebra.com.example.android.twitchandroid.fragments;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;

import zebra.com.example.android.twitchandroid.R;

/**
 * Created by zebra on 3/14/2015.
 */
public class StreamFragment extends Fragment implements MediaPlayer.OnPreparedListener, SurfaceHolder.Callback, MediaPlayer.OnErrorListener {

    private static final String LOG_TAG = StreamFragment.class.getSimpleName();

    private static final String STREAM_URL_KEY = "stream_url_key";
    private String streamUrl;
    private MediaPlayer mediaPlayer;
    private SurfaceHolder streamHolder;

    private SurfaceView mStreamVideo;
    private LinearLayout mButtonContainer;
    private Button mPlayButton;
    private Button mPauseButton;

    public StreamFragment() {
    }

    public static StreamFragment newInstance(String streamUrl) {
        Bundle args = new Bundle();
        args.putString(STREAM_URL_KEY, streamUrl);
        StreamFragment fragment = new StreamFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            streamUrl = getArguments().getString(STREAM_URL_KEY);
        } else {
            streamUrl = savedInstanceState.getString(STREAM_URL_KEY);
        }
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnErrorListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STREAM_URL_KEY, streamUrl);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stream, null);

        mStreamVideo = (SurfaceView) view.findViewById(R.id.vv_stream);
        streamHolder = mStreamVideo.getHolder();
        streamHolder.addCallback(this);
        mButtonContainer = (LinearLayout) view.findViewById(R.id.ll_button_container);
        mPlayButton = (Button) view.findViewById(R.id.btn_play);
        mPauseButton = (Button) view.findViewById(R.id.btn_pause);

        mButtonContainer.setVisibility(View.GONE);
        mStreamVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mButtonContainer.getVisibility() == View.GONE) {
                    mButtonContainer.setVisibility(View.GONE);
                } else {
                    mButtonContainer.setVisibility(View.VISIBLE);
                }
            }
        });
        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        mediaPlayer.stop();
    }

    private void loadStream() {
        try {
            mediaPlayer.setDataSource(streamUrl);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error loading stream " + mStreamVideo, e);
        }
        mediaPlayer.prepareAsync();

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.v(LOG_TAG, "Prepared for media playback");
        mediaPlayer.start();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mediaPlayer.setDisplay(holder);
        loadStream();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.v(LOG_TAG, "Surface changed");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.v(LOG_TAG, "Surface destroyed");
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Toast.makeText(getActivity(), "Caught error: " + what + "," + extra, Toast.LENGTH_SHORT).show();
        Log.e(LOG_TAG, "Caught error: " + what + "," + extra);
        mp.reset();
        return true;
    }
}
