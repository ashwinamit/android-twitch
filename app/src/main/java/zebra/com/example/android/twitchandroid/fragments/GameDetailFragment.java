package zebra.com.example.android.twitchandroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import zebra.com.example.android.twitchandroid.R;
import zebra.com.example.android.twitchandroid.model.TwitchStream;
import zebra.com.example.android.twitchandroid.tasks.DownloadStreamList;
import zebra.com.example.android.twitchandroid.tasks.TwitchDownloader;

/**
 * Created by zebra on 3/13/2015.
 */
public class GameDetailFragment extends ListFragment implements TwitchDownloader.DownloadListener<TwitchStream> {

    public interface GameDetailFragmentContainer {
        public void streamSelected(String streamName);
    }

    private static final String LOG_TAG = GameDetailFragment.class.getSimpleName();
    private static final String GAME_NAME_KEY = "game_name_key";


    private GameDetailFragmentContainer container;
    private ArrayAdapter<String> mStreamAdapter;
    private String gameName;

    public GameDetailFragment() {

    }

    public static GameDetailFragment newInstance(String gameName) {
        Bundle args = new Bundle();
        args.putString(GAME_NAME_KEY, gameName);

        GameDetailFragment fragment = new GameDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            container = (GameDetailFragmentContainer) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement GameDetailFragmentContainer");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        container = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState == null) {
            gameName = getArguments().getString(GAME_NAME_KEY);
        } else {
            gameName = savedInstanceState.getString(GAME_NAME_KEY);
        }
        mStreamAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        setListAdapter(mStreamAdapter);
        downloadStreams();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.streamlist, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            downloadStreams();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(GAME_NAME_KEY, gameName);
        super.onSaveInstanceState(outState);
    }

    private void downloadStreams() {
        Log.v(LOG_TAG, "Loading streams for " + gameName);
        new DownloadStreamList(this).execute(new DownloadStreamList.StreamQuery(gameName, 0, 100));
    }

    @Override
    public void downloadComplete(TwitchStream[] result) {
        mStreamAdapter.clear();
        if (result != null) {
            for (TwitchStream s : result) {
                mStreamAdapter.add(s.getName());
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String streamName = mStreamAdapter.getItem(position);
        Log.v(LOG_TAG, "Clicked on: " + streamName);
        container.streamSelected(streamName);
    }
}
