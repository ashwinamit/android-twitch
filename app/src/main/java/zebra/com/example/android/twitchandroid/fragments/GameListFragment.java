package zebra.com.example.android.twitchandroid.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import zebra.com.example.android.twitchandroid.R;
import zebra.com.example.android.twitchandroid.model.TwitchGame;
import zebra.com.example.android.twitchandroid.tasks.DownloadGameList;

/**
 * Created by zebra on 3/13/2015.
 */
public class GameListFragment extends ListFragment implements DownloadGameList.DownloadListener<TwitchGame> {

    public static interface GameListFragmentContainer {
        public void gameSelected(String gameName);
    }

    private static final String LOG_TAG = GameListFragment.class.getSimpleName();
    private static final String DOWNLOADED_GAMES = "downloaded_games";

    private ArrayAdapter<String> mGamesAdapter;
    private GameListFragmentContainer container;
    private TwitchGame[] games;

    public GameListFragment() {
    }

    public static GameListFragment newInstance() {
        return new GameListFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            container = (GameListFragmentContainer) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        container = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mGamesAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        Log.v(LOG_TAG, "onCreate");
        setListAdapter(mGamesAdapter);

        if (savedInstanceState != null) {
            this.games = (TwitchGame[]) savedInstanceState.getParcelableArray(DOWNLOADED_GAMES);
            this.gamesArrayUpdated();
        }
        downloadGames();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String gameName = mGamesAdapter.getItem(position);
        Log.v(LOG_TAG, gameName);
        container.gameSelected(gameName);

    }

    private void downloadGames() {
        new DownloadGameList(this).execute(new DownloadGameList.GameQuery(0, 100));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.gamelist, menu);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArray(DOWNLOADED_GAMES, games);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            Log.d(LOG_TAG, "Trying to pull 100 games");
            downloadGames();
        }
        return super.onOptionsItemSelected(item);
    }

    private void gamesArrayUpdated() {
        mGamesAdapter.clear();
        if (games != null) {
            for (TwitchGame g : games) {
                mGamesAdapter.add(g.getName());
            }
        }
    }

    @Override
    public void downloadComplete(TwitchGame[] result) {
        this.games = result;
        this.gamesArrayUpdated();
    }

}
